import React from 'react';
import './App.css';

import { getBaseData, getDataFromUrl } from './service/rickMortyApi';

class App extends React.Component {
  state = {
    characters: [],
    nextPage: null,
  };
  async componentDidMount() {
    const BaseData = await getBaseData();
    this.setState({
      characters: BaseData.results,
      nextPage: BaseData.nextPage,
    });
  }

  handleLoadData = async() => {
    const dataFromUrl = await getDataFromUrl(this.state.nextPage);
    this.setState((prevState) => ({
      characters: [...prevState.characters, ...dataFromUrl.results],
      nextPage: dataFromUrl.nextPage,
    }));
  };
  render() {
    console.log(this.state);
    return (
      <div className="App">
        <h1>EJERCICIO</h1>
        <ul>
          {this.state.characters.map((chart) => {
            return (
              <li key={chart.id}>
                <h3>Nombre: {chart.name}</h3>
                <img src={chart.image} alt={chart.name} />
              </li>
            );
          })}
        </ul>

        <button onClick={this.handleLoadData}>Cargar más</button>
      </div>
    );
  }
}

export default App;
